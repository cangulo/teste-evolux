<?php

$json = json_decode($_POST['cdr'], true);


$cdrToSave = array(
    'origem' => $json['variables']['sip_from_user'],
    'destino' => $json['variables']['sip_to_user'],
    'duracao' => $json['variables']['duration'],
);

if (!$cdrToSave['origem']) return;


$pdo = new PDO('mysql:host=localhost;dbname=evolux', 'root', 'myadmin');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

$sql = 'insert into cdr (origem, destino, duracao) values (?, ?, ?)';

$stmt = $pdo->prepare($sql);

$stmt->bindValue(1, $cdrToSave['origem']);
$stmt->bindValue(2, $cdrToSave['destino']);
$stmt->bindValue(3, $cdrToSave['duracao']);


$stmt->execute();

