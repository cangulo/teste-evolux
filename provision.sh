wget -O - https://files.freeswitch.org/repo/deb/debian/freeswitch_archive_g0.pub | apt-key add -
echo "deb http://files.freeswitch.org/repo/deb/freeswitch-1.6/ jessie main" > /etc/apt/sources.list.d/freeswitch.list
apt-get update
export DEBIAN_FRONTEND="noninteractive"
debconf-set-selections <<< 'mysql-server mysql-server/root_password password myadmin'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password myadmin'
apt-get install -y --force-yes freeswitch-meta-all freeswitch-mod-json-cdr freeswitch-mod-callcenter mysql-server mysql-client php5 php5-mysql supervisor
export FREESWITCH_VM_IP=$(cat /vagrant/Vagrantfile  | grep -i "private_network" | head -n1 | awk -F '"' '{print $4}')
sed -i "s@<include>@<include>\n  <X-PRE-PROCESS cmd=\"set\" data=\"local_ip_v4=$FREESWITCH_VM_IP\"/>@g" /etc/freeswitch/vars.xml
sed -i 's@</modules>@  <load module="mod_json_cdr"/>\n    <load module="mod_callcenter"/>\n  </modules>@g' /etc/freeswitch/autoload_configs/modules.conf.xml
cp /vagrant/json_cdr.conf.xml /etc/freeswitch/autoload_configs
cp /vagrant/callcenter.conf.xml /etc/freeswitch/autoload_configs
cp /vagrant/100.xml /etc/freeswitch/directory/default
cp /vagrant/101.xml /etc/freeswitch/directory/default
cp /vagrant/102.xml /etc/freeswitch/directory/default
cp /vagrant/default.xml /etc/freeswitch/dialplan
cat /vagrant/callcenter.conf.xml  | sed "s/FREESWITCH_VM_IP/$FREESWITCH_VM_IP/g" > /etc/freeswitch/autoload_configs/callcenter.conf.xml
cp /vagrant/post.php /var/www/html
mysql -u root -pmyadmin -e "create database evolux character set utf8 collate utf8_unicode_ci"
mysql -uroot -pmyadmin evolux < /vagrant/db.sql
cd /usr/local/bin
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
ln -s composer.phar composer
cp -R /vagrant/logserver /var/www/html
cp /vagrant/ratchet.conf /etc/supervisor/conf.d
cd /var/www/html/logserver
composer install
service freeswitch restart
service apache2 restart
systemctl enable supervisor
systemctl start supervisor
service supervisor restart
