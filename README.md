# Freeswitch VM - teste evolux

Esta é uma máquina virtual Debian 8 + FreeSwitch, configurada de acordo com os seguintes parâmetros:


1. Instalação do FreeSWITCH em Debian 8 (VM)
2. Configuração de 3 ramais em softphone: 100, 101, 102
3. Ligações entre ramais precisam ser gravadas
4. Implementação de fila de atendimento com número 8000 (utilizar mod_callcenter)
5. Possibilidade de efetuar uma chamada a partir do ramal 100 para o número 8000 e ser atendido por um agente logado no ramal 102.
6. Capturar CDRs das chamadas com mod_json_cdr, postar para uma URL.
7. Salvar os dados da chamada em um banco mysql: Origem, Destino, Duração da conversação.
8. Exibir os dados do mysql em página web feita em AngularJS.


Autor: Eduardo Marinho

### Instalação

```sh
$ vagrant up
```

Quando o vagrant terminar de provisionar, a máquina freeswitch estará disponível no IP 192.168.33.11.

Caso se deseje outro IP, basta alterar a linha 29 do Vagrantfile.
