<?php
require 'vendor/autoload.php';

use Ratchet\Http\HttpServer;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;


class Monitor implements MessageComponentInterface {
    public $clients;

    public $pdo;

    public function __construct() {
        $this->clients = new \SplObjectStorage;

        $this->pdo = new PDO('mysql:host=localhost;dbname=evolux', 'root', 'myadmin');

    }

    public function onOpen(ConnectionInterface $conn) {
        // Store the new connection to send messages to later

        $item = new StdClass;
        $item->connection = $conn;
        $item->ts = time();

        $this->clients->attach($item);

        $sql = 'select * from cdr order by criado_em asc';
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();


        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        $conn->send(json_encode($result, JSON_OBJECT_AS_ARRAY));

    }

    /**
     * This is called before or after a socket is closed (depends on how it's closed).  SendMessage to $conn will not result in an error if it has already been closed.
     * @param  ConnectionInterface $conn The socket/connection that is closing/closed
     * @throws \Exception
     */
    function onClose(ConnectionInterface $conn)
    {
        // TODO: Implement onClose() method.
    }

    /**
     * If there is an error with one of the sockets, or somewhere in the application where an Exception is thrown,
     * the Exception is sent back down the stack, handled by the Server and bubbled back up the application through this method
     * @param  ConnectionInterface $conn
     * @param  \Exception $e
     * @throws \Exception
     */
    function onError(ConnectionInterface $conn, \Exception $e)
    {
        // TODO: Implement onError() method.
    }

    /**
     * Triggered when a client sends data through the socket
     * @param  \Ratchet\ConnectionInterface $from The socket/connection that sent the message to your application
     * @param  string $msg The message received
     * @throws \Exception
     */
    function onMessage(ConnectionInterface $from, $msg)
    {
        // TODO: Implement onMessage() method.
    }
}


$mon = new Monitor;

$server = IoServer::factory(
    new HttpServer(
        new WsServer(
            $mon
        )
    ),
    8080
);


$server->loop->addPeriodicTimer(10, function() use ($mon) {
    $sql = 'select * from cdr where criado_em > ? order by criado_em asc';
    $time = time();

    foreach ($mon->clients as $conn) {
        $stmt = $mon->pdo->prepare($sql);
        $stmt->bindValue(1, date('Y-m-d H:i:s', $conn->ts));
        $stmt->execute();

        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        if ($result) {
            $r = json_encode($result, JSON_OBJECT_AS_ARRAY);
            $conn->ts = $time;
            $conn->connection->send($r);
        }

    }
});


$server->run();
