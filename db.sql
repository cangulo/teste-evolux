create table cdr (
    id bigint primary key auto_increment, 
    origem text,
    destino text,
    duracao text,
    criado_em timestamp default current_timestamp
);
